#include "render.h"
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include <math.h>
#include <stdio.h>

double scale_factor, off_x, off_y;

void update_scale(int w_, int h_) {
	double w = (double)w_;
	double h = (double)h_;

	// find the biggest factor gf that satisfies
	// - (gf * 640) < w
	// - (gf * 480) < h
	double gf = ((w / 640) < (h / 480) ? (w / 640) : (h / 480));

	double cw = 640 * gf;
	double ch = 480 * gf;
	scale_factor = gf;
	off_x = w / 2 - cw / 2;
	off_y = h / 2 - ch / 2;
}

int scale(double v) {
	return (int)(v * scale_factor);
}
double unscale(int v) {
	return (double)v / scale_factor;
}
void game_to_screen(int* tx, int* ty, double x, double y) {
	*tx = (int)(x * scale_factor + off_x);
	*ty = (int)(y * scale_factor + off_y);
}
void screen_to_game(double* tx, double* ty, int x, int y) {
	*tx = ((double)x - off_x) / scale_factor;
	*ty = ((double)y - off_y) / scale_factor;
}

const uint segments = 36;
SDL_Point points[36];

void draw_circle(double cx, double cy, double radius) {
	for (uint i = 0; i < segments; i++) {
		double a = (double)i / (double)(segments - 1) * PI * 2;
		double x = cx + sin(a) * radius;
		double y = cy + cos(a) * radius;
		game_to_screen(&points[i].x, &points[i].y, x, y);
	}
	SDL_RenderDrawLines(renderer, points, segments);
}

// TODO
void draw_circle_filled(double cx, double cy, double radius) {
	draw_circle(cx, cy, radius);
}
