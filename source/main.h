#pragma once
#include <SDL2/SDL.h>
#include "beatmap.h"

typedef struct Game_s {
	int width, height;
	int time;

    int approach_time;
    int max_time_error;
    int extra_show_time;
    double circle_radius;
    double approach_circle_radius_factor;

} Game;


extern char quit;
extern Game* game;
extern Beatmap* beatmap;


void loop();

Result test();

