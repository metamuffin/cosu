#include "beatmap.h"
#include "helper.h"
#include <string.h>
#include <stdio.h>
#include "helper.h"

#define ASSERT(cond, ...) R_ERROR_UNLESS(cond, "test: " __VA_ARGS__)

Result test() {
	char* test_str = "ABCDEFGHI";
	char* test_str_end = test_str + strlen(test_str);

	ASSERT(find_next(test_str, 'D', test_str_end) == test_str + 3,
	       "find something that exists");
	ASSERT(find_next(test_str, 'B', test_str_end) == test_str + 1,
	       "find something that exists");
	ASSERT(find_next(test_str, '?', test_str_end) == NULL,
	       "find something that does not exists");
	ASSERT(find_next(test_str, '\0', test_str_end) == NULL, "find null");

	ASSERT(strstarts(test_str, "ABC", test_str_end), "")
	ASSERT(strstarts(test_str, "", test_str_end), "strstarts on empty")
	ASSERT(strstarts(test_str, "ABCDEFGHI", test_str_end), "strstarts on all")
	ASSERT(strstarts(test_str, "ABCDEFGHIJK", test_str_end), "strstarts on more than all")
	ASSERT(!strstarts(test_str, "ABD", test_str_end), "")
	ASSERT(!strstarts(test_str, "X", test_str_end), "")

	char* test_str2 = "Key: Value";
	char* test_str2_end = test_str2 + strlen(test_str2);

	char* a;
	ASSERT((a = get_prop(test_str2, test_str2_end, "Key")), "get propery")
	ASSERT(!strcmp(a, " Value"), "get property (value)")
	ASSERT(!get_prop(test_str2, test_str2_end, "Notkey"), "get propery (non-existent)")
	ASSERT(get_prop(test_str2, test_str2_end, ""), "get propery (empty)")

	char* test_section = "AudioFilename: abc\nAudioLeadIn: 42\nKey: Value\n";
	char* test_section_end = test_section + strlen(test_section);

	Beatmap beatmap;
	ASSERT(!(section_general(&beatmap, test_section, test_section_end)),
	       "parse general section")
	ASSERT(beatmap.general_audio_filename, "audio filename exists")
	ASSERT(!strcmp(beatmap.general_audio_filename, " abc"), "audio filename correct")
	ASSERT(beatmap.general_audio_lead_in == 42, "audio lead in correct")

	char* test_section2 = "991,992,992,1,0,0:0:0:0:\n0,0,10630,6,0,L|22:95,1,45\n428,136,9972,1,0,0:0:0:0:\n";
	char* test_section2_end = test_section2 + strlen(test_section2);

	ASSERT(!(section_hit_objects(&beatmap, test_section2, test_section2_end)),
	       "parse hit objects")
	ASSERT(beatmap.hit_object_count == 2, "hit object count")
	ASSERT(beatmap.hit_objects, "hit object array")
	ASSERT(beatmap.hit_objects[0]->x == 991, "x")
	ASSERT(beatmap.hit_objects[0]->y == 992, "y")

	printf("tests ok\n");
	return OK;
}
