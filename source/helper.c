
#include "time.h"
#include "helper.h"

int time_d() {
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC_RAW, &t);
	return t.tv_sec * 1000000 + t.tv_nsec / 1000;
}

