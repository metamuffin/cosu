#include "beatmap.h"
#include <SDL2/SDL.h>
#include "main.h"
#include "render.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "audio.h"

int height = 640;
int width = 480;

Beatmap* beatmap;
Game* game;

int main(int argc, char** argv) {
	R_ERROR_IF(
	        argc < 2,
	        "not enough arguments, provide a path to a beatmap as the first argument, or --test to test")
	if (!strcmp(argv[1], "--test")) {
		R_ERROR_IF(test() != OK, "tests failed");
		return 0;
	}

	char* filename = argv[1];
	beatmap = load_beatmap(filename);
	R_ERROR_UNLESS(beatmap, "failed to load beatmap: %s\n", filename);

	game = malloc(sizeof(Game));
	game->time = 0;
	game->width = 640;
	game->height = 480;

	game->approach_time = 1000000;
	game->circle_radius = 40;
	game->approach_circle_radius_factor = 4;
	game->max_time_error = 200000;
	game->extra_show_time = 500000;

	R_ERROR_IF(init_render(), "failed to initialize renderer\n");
	R_ERROR_IF(init_audio(), "failed to initialize renderer\n");

	start_audio();
	loop();

	quit_render();
	quit_audio();

	return 0;
}



char quit = 0;
void loop() {
	SDL_Event event;
	while (!quit) {
		game->time = time_d();
		while (SDL_PollEvent(&event) > 0) {
			switch (event.type) {
			case SDL_QUIT:
				quit = 1;
				break;
			case SDL_KEYDOWN:
				// printf("keydown %i\n", event.key.keysym.sym);
				break;
			case SDL_KEYUP:
				// printf("keyup %i\n", event.key.keysym.sym);
				break;
			case SDL_MOUSEMOTION:
				// printf("motion: %i %i\n", event.motion.x, event.motion.y);
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (event.button.button == SDL_BUTTON_LEFT) {
					double mx, my;
					screen_to_game(&mx, &my, event.button.x, event.button.y);
					mouse_hit(mx, my);
				}
				break;
			case SDL_WINDOWEVENT:
				if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
					update_scale(event.window.data1, event.window.data2);
				}
				break;
			}
		}
		update();
		draw();
		SDL_Delay(16);
	}
}
