// hello world!
#include "helper.h"
#include "beatmap.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

void print_slice(char* start, char* end) {
	for (char* p = start; p < end; p++) {
		printf("%c", *p);
	}
	printf("\n");
}

char* find_next(char* b, char c, char* end) {
	while (b < end && *b++ != c) {
	}
	if (b >= end)
		return NULL;
	return b - 1;
}

int find_next_off(char* b, char c, char* end) {
	char* s = b;
	while (b < end && *b++ != c) {
	}
	if (b == end)
		return -1;
	return b - s;
}

int strstarts(char* a, const char* b, char* end) {
	while (a < end && *b) {
		if (*a++ != *b++)
			return 0;
	}
	return 1;
}

// allocate a string if line starts with the property name
char* get_prop(char* line, char* line_end, char* name) {
	char* del = find_next(line, ':', line_end);
	if (del == NULL)
		return NULL;
	if (!strstarts(line, name, del))
		return NULL;
	char* value_start = del + 1;
	char* value = malloc(line_end - value_start + 1);
	P_ERROR_UNLESS(value, "malloc failed");
	for (char *s = value_start, *d = value; s < line_end; s++, d++)
		*d = *s;
	value[line_end - value_start] = '\x00';
	return value;
}

Result section_general(Beatmap* target, char* section_start, char* section_end) {
	char* c = section_start;

	while (c <= section_end) {
		char* line_end = find_next(c, '\n', section_end);
		if (line_end == NULL)
			break;
		char* value;
		if ((value = get_prop(c, line_end, "AudioFilename"))) {
			target->general_audio_filename = value;
		}
		else if ((value = get_prop(c, line_end, "AudioLeadIn"))) {
			target->general_audio_lead_in = atoi(value);
			free(value);
		}
		c = line_end + 1;
	}

	return OK;
}

char* read_field(char* line_start, char* line_end, char del, char index) {
	char* c = line_start;
	for (char i = 0; i < index; i++) {
		c = find_next(c, del, line_end);
		P_ERROR_UNLESS(c, "failed to seek next delimeter while reading field %i", index)
		c++;
	}
	char* vs_start = c;
	char* vs_end = find_next(c, del, line_end);
	if (vs_end == NULL)
		vs_end = line_end;
	char* value = malloc(vs_end - vs_start + 1);
	P_ERROR_UNLESS(value, "malloc in read_field of size %i failed",
	               (int)(vs_end - vs_start + 1));
	for (uint i = 0; i < vs_end - vs_start; i++)
		value[i] = vs_start[i];
	value[vs_end - vs_start] = '\0';
	return value;
}

// todo this can be optimized to not call read_field, useless malloc
Result read_field_int(int* target, char* line_start, char* line_end, char del, char index) {
	char* value = read_field(line_start, line_end, del, index);
	R_ERROR_UNLESS(value, "read of field %i as integer failed", index);
	*target = atoi(value);
	free(value);
	return OK;
}

Result parser_hitobject_slider_count_params(char* params, char* params_end) {
	uint param_count = 0;
	char* d = params;
	while (1) {
		d = find_next(d, '|', params_end);
		param_count++;
		if (d == NULL)
			break;
		d++;
	}
	R_ERROR_IF(param_count <= 1, "slider hit object requires at least 2 parameters");
	// printf("%s -> %i\n", params, param_count);
	return OK;
}

Result parse_hitobject_slider_type(HitObject* hitobj, char** cursor) {
	hitobj->curve_type = **cursor;
	(*cursor)++;
	R_ERROR_UNLESS(**cursor == '|', "expected seperator in 2nd col of object params");
	(*cursor)++;
	return OK;
}

Result hitobj_add_point(HitObject* hitobj, int x, int y) {
	hitobj->point_count++;
	hitobj->points = realloc(hitobj->points, sizeof(HitSliderCurvePoint) * hitobj->point_count);
	R_ERROR_UNLESS(hitobj->points, "realloc in hit_object_section (params) of size %i failed.",
	               (int)sizeof(HitSliderCurvePoint) * hitobj->point_count);
	hitobj->points[hitobj->point_count - 1].x = x;
	hitobj->points[hitobj->point_count - 1].y = y;
	return OK;
}

Result hitobj_params_read_coords(char* p, char* p_end, int* x, int* y) {
	char* xr = read_field(p, p_end, ':', 0);
	R_ERROR_UNLESS(xr, "could not parse x component");
	char* yr = read_field(p, p_end, ':', 1);
	R_ERROR_UNLESS(yr, "could not parse y component");
	*x = atoi(xr);
	*y = atoi(yr);
	return OK;
}

Result parse_hitobject_slider(HitObject* hitobj, char* c, char* line_end) {
	char* params = read_field(c, line_end, ',', 5);
	R_ERROR_UNLESS(params, "read params");
	char* params_end = params + strlen(params);
	R_ERROR_IF(parser_hitobject_slider_count_params(params, params_end), "blub");

	char* cursor = params;
	R_ERROR_IF(parse_hitobject_slider_type(hitobj, &cursor), "type");
	hitobj->point_count = 0;
	hitobj->points = NULL;
	while (1) {
		char* p = cursor;
		cursor = find_next(cursor, '|', params_end);
		char* p_end = cursor ? cursor : params_end;
		int x, y;
		R_ERROR_IF(hitobj_params_read_coords(p, p_end, &x, &y), "read coords");
		R_ERROR_IF(hitobj_add_point(hitobj, x, y), "add point\n");
		// LOG_VERBOSE("\t |--i:%i\tx:%i\ty:%i\n", hitobj->point_count, x, y);
		if (cursor == NULL)
			break;
		cursor++;
	}
	free(params);
	return OK;
}

Result parse_hitobject_fields(HitObject* hitobj, char* c, char* line_end) {
	R_ERROR_IF(read_field_int(&hitobj->x, c, line_end, ',', 0),
	           "failed to parse x coordinate of hit object");
	R_ERROR_IF(read_field_int(&hitobj->y, c, line_end, ',', 1),
	           "failed to parse y coordinate of hit object");
	R_ERROR_IF(read_field_int(&hitobj->time, c, line_end, ',', 2),
	           "failed to parse time of hit object");
	R_ERROR_IF(read_field_int((int*)&hitobj->type, c, line_end, ',', 3),
	           "failed to parse type of hit object");
	return OK;
}

Result parse_hitobject_line(HitObject* hitobj, char* c, char* line_end) {
	R_ERROR_IF(parse_hitobject_fields(hitobj, c, line_end),
	           "failed to read hitobject fields\n");

	// LOG_VERBOSE("\tx:%i\ty:%i\ttype:%i\ttime:%i\n", hitobj->x, hitobj->y, hitobj->type,
	//             hitobj->time);
	if (hitobj->type & HITOBJECT_TYPE_SLIDER) {
		R_ERROR_IF(parse_hitobject_slider(hitobj, c, line_end), "failed to parse slider")
	}
	hitobj->time *= 1000; // abjust unit to micros
	return OK;
}

Result section_hit_objects(Beatmap* beatmap, char* section_start, char* section_end) {
	char* c = section_start;
	beatmap->hit_objects = NULL;
	beatmap->hit_object_count = 0;

	while (c < section_end) {
		char* line_end = find_next(c, '\n', section_end);
		if (line_end == NULL)
			break;

		HitObject* hitobj = malloc(sizeof(HitObject));
		R_ERROR_IF(parse_hitobject_line(hitobj, c, line_end),
		           "failed to parse hitobject line\n");

		beatmap->hit_object_count++;
		beatmap->hit_objects
		        = realloc(beatmap->hit_objects, sizeof(HitObject*) * beatmap->hit_object_count);
		R_ERROR_IF(beatmap->hit_objects == NULL,
		           "realloc in section_hit_objects of size %i failed",
		           (int)sizeof(HitObject*) * beatmap->hit_object_count);
		beatmap->hit_objects[beatmap->hit_object_count - 1] = hitobj;
		c = line_end + 1;
	}
	return OK;
}

Beatmap* load_beatmap(char* filename) {
	int fd = open(filename, O_RDONLY | O_CLOEXEC);
	P_ERROR_IF(fd < 0, "could not open beatmap file\n");
	struct stat file_info;
	P_ERROR_IF(fstat(fd, &file_info) < 0, "fstat on beatmap failed");
	uint size = file_info.st_size;
	char* m = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	P_ERROR_IF(m == MAP_FAILED, "mmap for beatmap failed");
	Beatmap* b = malloc(sizeof(Beatmap));
	P_ERROR_IF(b == NULL, "beatmap malloc failed")

	char* c = m;
	char* file_end = m + size;
	c = find_next(m, '[', file_end);
	if (c == NULL)
		return NULL;
	c++;

	while (c != file_end) {
		char* section_name = c;
		c = find_next(c, ']', file_end); // todo safety checks
		P_ERROR_UNLESS(c, "failed to find section header ending at offset %i",
		               (int)(section_name - m));
		c++; // skip closing bracket
		P_ERROR_UNLESS(*c == '\n', "expected newline, found %c (%i)", *c, *c);
		c++; // skip newline
		char* section_start = c;
		c = find_next(c, '[', file_end);
		if (c == NULL) // last section
			c = file_end - 1;
		c++;
		char* section_end = c - 1;
		if (strstarts(section_name, "Editor", file_end)
		    || strstarts(section_name, "Colours", file_end)
		    || strstarts(section_name, "Events", file_end))
		{
			continue;
		}
		if (strstarts(section_name, "General", file_end)) {
			P_ERROR_IF(section_general(b, section_start, section_end),
			           "failed to parse general section");
		}
		else if (strstarts(section_name, "TimingPoints", file_end)) {
		}
		else if (strstarts(section_name, "Metadata", file_end)) {
		}
		else if (strstarts(section_name, "HitObjects", file_end)) {
			P_ERROR_IF(section_hit_objects(b, section_start, section_end),
			           "failed to parse hit objects section")
		}
		else if (strstarts(section_name, "Difficulty", file_end)) {
		}
		else if (strstarts(section_name, "TimingPoints", file_end)) {
		}
		else {
			printf("unknown section ahead\n");
			return NULL;
		}
	}
	return b;
}
