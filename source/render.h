#pragma once
#include "logic.h"
#include "helper.h"
#include <SDL2/SDL.h>
#include "beatmap.h"

extern SDL_Window* window;
extern SDL_Surface* surface;
extern SDL_Renderer* renderer;

Result init_render();
void quit_render();

void draw();

void update_scale();
void game_to_screen(int* tx, int* ty, double x, double y);
void screen_to_game(double* tx, double* ty, int x, int y);
int scale(double v);
double unscale(int v);

void draw_circle(double cx, double cy, double radius);
void draw_circle_filled(double cx, double cy, double radius);

void draw_hit_object(HitObject* o);
void draw_hit_circle(HitObject* o);
void draw_slider(HitObject* o);
void draw_spinner(HitObject* o);
