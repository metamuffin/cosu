
#include "main.h"
#include <SDL2/SDL_mixer.h>
#include "helper.h"

Mix_Music* music;
Result init_audio() {
	int flags = MIX_INIT_OGG | MIX_INIT_OPUS | MIX_INIT_MP3;
	R_ERROR_IF(Mix_Init(flags) != flags, "failed to init sdl2_mixer")

	Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 640);
	music = Mix_LoadMUS(/*beatmap->general_audio_filename*/ "beatmaps/audio.mp3");

	return OK;
}

void start_audio() {
	Mix_PlayMusic(music, 1);
}

void quit_audio() {
	Mix_FreeMusic(music);
}