#pragma once
#include "main.h"

extern uint active_range_start;
extern uint active_range_end;

void mouse_hit(double mx, double my);
void hitobject_hit(HitObject* h, int dt);
void update();