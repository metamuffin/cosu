#include "beatmap.h"
#include "main.h"
#include "render.h"
#include "helper.h"
#include "logic.h"
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_mouse.h>
#include <SDL2/SDL_video.h>
#include <stdio.h>

SDL_Window* window;
SDL_Surface* surface;
SDL_Renderer* renderer;

int last_time;

Result init_render() {
	R_ERROR_IF(SDL_Init(SDL_INIT_VIDEO) < 0, "failed to initialize sdl2\n");
	R_ERROR_UNLESS((window = SDL_CreateWindow("cosu", SDL_WINDOWPOS_CENTERED,
	                                          SDL_WINDOWPOS_CENTERED, game->width, game->height,
	                                          SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE)),
	               "failed to create window\n");
	R_ERROR_UNLESS((renderer = SDL_CreateRenderer(window, -1, 0)),
	               "failed to create renderer\n");
	update_scale(game->width, game->height);

	for (int i = 0; i < beatmap->hit_object_count; i++) {
		beatmap->hit_objects[i]->was_hit = 0;
	}

	return OK;
}

void quit_render() {
	free(beatmap);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void draw() {
	int current_time = time_d();
	int delta = current_time - last_time;
	last_time = current_time;

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	game->time += delta;

	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	for (uint i = active_range_start; i < active_range_end; i++) {
		HitObject* h = beatmap->hit_objects[i];
		draw_hit_object(h);
	}

	SDL_RenderPresent(renderer);
}

void draw_hit_object(HitObject* o) {
	if (o->type & HITOBJECT_TYPE_CIRCLE)
		draw_hit_circle(o);
	else if (o->type & HITOBJECT_TYPE_SLIDER)
		draw_slider(o);
	else if (o->type & HITOBJECT_TYPE_SPINNER)
		draw_spinner(o);
	else
		printf("WARN: unknown hit object type: %i\n", o->type);
}

void draw_hit_circle(HitObject* o) {
	int tdelta = o->time - game->time;

	if (o->was_hit)
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
	else if (game->time > o->time + game->max_time_error)
		SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	else if (game->time < o->time - game->max_time_error)
		SDL_SetRenderDrawColor(renderer, 150, 0, 150, 255);
	else
		SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255);

	draw_circle(o->x, o->y, game->circle_radius);

	SDL_SetRenderDrawColor(renderer, 0, 100, 100, 255);
	double aw = (double)tdelta / game->approach_time;
	double af = aw * game->approach_circle_radius_factor + (1 - aw);
	if (af >= 0 && !o->was_hit)
		draw_circle(o->x, o->y, game->circle_radius * af);
}

void draw_slider(HitObject* o) {
	draw_hit_circle(o);
}

void draw_spinner(HitObject* o) {
	draw_hit_circle(o);
}
