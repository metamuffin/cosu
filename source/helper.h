#pragma once

#define MACRO_STR_(s) #s
#define MACRO_STR(s) MACRO_STR_(s)
#define ERROR_RETURN_IF(value, cond, ...)                                    \
	if (cond) {                                                              \
		printf("ERROR:\t" __VA_ARGS__);                                      \
		printf("\n\t'" #cond "' in " __FILE__ ":" MACRO_STR(__LINE__) "\n"); \
		return (value);                                                      \
	}
#define ERROR_RETURN_UNLESS(value, cond, ...) ERROR_RETURN_IF(value, !(cond), __VA_ARGS__)

#define P_ERROR_IF(cond, ...) ERROR_RETURN_IF(NULL, cond, __VA_ARGS__)
#define R_ERROR_IF(cond, ...) ERROR_RETURN_IF(1, cond, __VA_ARGS__)
#define I_ERROR_IF(cond, ...) ERROR_RETURN_IF(-1, cond, __VA_ARGS__)
#define P_ERROR_UNLESS(cond, ...) ERROR_RETURN_UNLESS(NULL, cond, __VA_ARGS__)
#define R_ERROR_UNLESS(cond, ...) ERROR_RETURN_UNLESS(1, cond, __VA_ARGS__)
#define I_ERROR_UNLESS(cond, ...) ERROR_RETURN_UNLESS(-1, cond, __VA_ARGS__)

#define LOG_VERBOSE(...) printf(__VA_ARGS__)

#define streq(a, b) (!strcmp(a, b))

#define unless(cond) if (!(cond))

#define INT_ERROR (1 << 63)
#define PI 3.14159

typedef unsigned int uint;

typedef enum Result_e { OK = 0, ERROR } Result;

int time_d();
