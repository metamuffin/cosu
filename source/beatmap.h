#pragma once
#include "helper.h"

typedef struct Event_s {
	int type;
	int start;
	char* params;
} Event;

typedef struct Break_s {
	int start;
	int end;
} Break;

typedef struct TimingPoint_s {
	int time;
	int beat_length;
	int meter;
	int sample_set;
	int sample_index;
	int volume;
	int uninherited;
	int effects;
} TimingPoint;

#define HITOBJECT_TYPE_CIRCLE (1 << 0)
#define HITOBJECT_TYPE_SLIDER (1 << 1)
#define HITOBJECT_TYPE_SPINNER (1 << 3)
#define HITOBJECT_TYPE_COMBO (1 << 2)

typedef struct HitSliderCurvePoint_s {
	int x;
	int y;
} HitSliderCurvePoint;

typedef struct HitObject_s {
	int x;
	int y;
	int time;
	char type;
	int was_hit;
	union {
		struct {
			char curve_type;
			uint point_count;
			HitSliderCurvePoint* points;
		};
		int spinner_end_time;
	};
} HitObject;

typedef struct Beatmap_s {
	char* general_audio_filename;
	int general_audio_lead_in;
	char* meta_title;
	char* meta_artist;
	char* meta_creator;
	int beatmapset_id;
	int beatmap_id;

	double difficulty_hp_drain_rate;
	double difiiculty_circle_size;
	double difficulty_overall_difficulty;
	// ...

	uint timing_point_count;
	TimingPoint** timing_points;

	uint event_count;
	Event** events;

	uint hit_object_count;
	HitObject** hit_objects;

} Beatmap;

Beatmap* load_beatmap(char* filename);

void print_slice(char* start, char* end);
char* find_next(char* b, char c, char* end);
int strstarts(char* a, const char* b, char* end);
int find_next_off(char* b, char c, char* end);
char* get_prop(char* line, char* line_end, char* name);
Result section_general(Beatmap* target, char* section_start, char* section_end);
char* read_field(char* line_start, char* line_end, char del, char index);
Result read_field_int(int* target, char* line_start, char* line_end, char del, char index);
Result parser_hitobject_slider_count_params(char* params, char* params_end);
Result parse_hitobject_slider_type(HitObject* hitobj, char** cursor);
Result hitobj_add_point(HitObject* hitobj, int x, int y);
Result hitobj_params_read_coords(char* p, char* p_end, int* x, int* y);
Result parse_hitobject_slider(HitObject* hitobj, char* c, char* line_end);
Result parse_hitobject_fields(HitObject* hitobj, char* c, char* line_end);
Result parse_hitobject_line(HitObject* hitobj, char* c, char* line_end);
Result section_hit_objects(Beatmap* beatmap, char* section_start, char* section_end);
Beatmap* load_beatmap(char* filename);
