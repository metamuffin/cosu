#include "logic.h"
#include "beatmap.h"
#include "main.h"
#include <stdio.h>

uint active_range_start = 0;
uint active_range_end = 0;

void update() {
	int pre = game->approach_time;
	int post = game->extra_show_time;
	for (uint i = active_range_start; i < beatmap->hit_object_count; i++) {
		HitObject* h = beatmap->hit_objects[i];
		active_range_end = i;
		if (h->time + post > game->time) {
			break;
		}
		if (h->time - pre < game->time) {
			active_range_start++;
			if (active_range_start >= beatmap->hit_object_count)
				quit = 1;
			continue;
		}
	}
	printf("active range: %i..%i\n", active_range_start, active_range_end);
}

void hitobject_hit(HitObject* h, int dt) {
	printf("hit: %i\n", dt);
	h->was_hit = 1;
}

void mouse_hit(double mx, double my) {
	for (uint i = active_range_start; i < active_range_end; i++) {
		HitObject* h = beatmap->hit_objects[i];
		double ds = sqrt((mx - h->x) * (mx - h->x) + (my - h->y) * (my - h->y));
		int dt = game->time - h->time;
		if (dt < 0)
			dt = -dt;
		if (ds <= game->circle_radius && dt < game->max_time_error && !h->was_hit) {
			hitobject_hit(h, dt);
		}
	}
}
