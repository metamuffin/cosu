#!/bin/make -f

CC = gcc
CFLAGS = -Wall
LDFLAGS = -lSDL2 -lSDL2_mixer -lm -pthread

EXECUTABLE := cosu
HEADERS := $(shell find source/ -type f -name '*.h')
SOURCES := $(shell find source/ -type f -name '*.c')
OBJECTS := $(SOURCES:source/%.c=build/%.o)

debug: CFLAGS += -Wextra -Wpedantic -g -Og
release: CFLAGS += -O3
release: LDFLAGS += -s
debug: $(EXECUTABLE)
release: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

build/%.o: source/%.c $(HEADERS)
	mkdir -p "$(@D)"
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf build
	rm -rf vgcore.*

